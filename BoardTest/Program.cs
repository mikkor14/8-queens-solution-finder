﻿using System;
using System.Collections.Generic;

namespace BoardTest
{
    class Program
    {

        static int N = 8;
        static int k = 1;

        static void PrintSolution(int[,] board)
        {
            Console.Write("{0}-\n", k++);
            for (int i = 0; i < N; i++)
            {
                for (int j = 0; j < N; j++)
                    Console.Write(" {0} ", board[i, j]);
                Console.Write("\n");
            }
            Console.Write("\n");
        }

        /* A utility function to check if a queen can  
        be placed on board[row,col]. Note that this  
        function is called when "col" queens are  
        already placed in columns from 0 to col -1.  
        So we need to check only left side for  
        attacking queens */
        static bool IsSafe(int[,] board, int row, int col)
        {
            int i, j;

            /* Check this row on left side */
            for (i = 0; i < col; i++)
                if (board[row, i] == 1)
                    return false;

            /* Check upper diagonal on left side */
            for (i = row, j = col; i >= 0 && j >= 0; i--, j--)
                if (board[i, j] == 1)
                    return false;

            /* Check lower diagonal on left side */
            for (i = row, j = col; j >= 0 && i < N; i++, j--)
                if (board[i, j] == 1)
                    return false;

            return true;
        }

        /* A recursive utility function  
        to solve N Queen problem */
        static bool SolveNQUtil(int[,] board, int col, String userInput)
        {
            int column = ConvertCoordinateLetter(userInput);
            int row = ConvertCoordinateNumber(userInput);
            /* base case: If all queens are placed  
            then return true */
            if (col == N)
            {
                CheckForSolution(row, column, board);
                return true;
            }

            /* Consider this column and try placing  
            this queen in all rows one by one */
            bool res = false;
            for (int i = 0; i < N; i++)
            {
                /* Check if queen can be placed on  
                board[i,col] */
                if (IsSafe(board, i, col))
                {
                    /* Place this queen in board[i,col] */
                    board[i, col] = 1;

                    // Make result true if any placement  
                    // is possible  
                    res = SolveNQUtil(board, col + 1, userInput) || res;

                    /* If placing queen in board[i,col]  
                    doesn't lead to a solution, then  
                    remove queen from board[i,col] */
                    board[i, col] = 0; // BACKTRACK  


                }

            }

            /* If queen can not be place in any row in  
                this column col then return false */
            return res;
        }

        /* This function solves the N Queen problem using  
        Backtracking. It mainly uses solveNQUtil() to  
        solve the problem. It returns false if queens  
        cannot be placed, otherwise return true and  
        prints placement of queens in the form of 1s.  
        Please note that there may be more than one  
        solutions, this function prints one of the  
        feasible solutions.*/
        static void SolveNQ(String userInput)
        {
            int[,] board = new int[N, N];

            if (SolveNQUtil(board, 0, userInput) == false)
            {
                Console.Write("Solution does not exist");
                return;
            }

            return;
        }


        static int ConvertCoordinateLetter(String userInput)
        {

            int result;

            switch (userInput[0])
            {
                case 'a':
                    result = 0;
                    break;
                case 'b':
                    result = 1;
                    break;
                case 'c':
                    result = 2;
                    break;
                case 'd':
                    result = 3;
                    break;
                case 'e':
                    result = 4;
                    break;
                case 'f':
                    result = 5;
                    break;
                case 'g':
                    result = 6;
                    break;
                case 'h':
                    result = 7;
                    break;
                default:
                    result = 0;
                    break;

            }

            return result;

        }

        static int ConvertCoordinateNumber(String userInput)
        {
            int result;

            result = 8 - Convert.ToInt32(Char.GetNumericValue(userInput[1]));

            return result;
        }

        static void CheckForSolution(int row, int col, int[,] board)
        {

            if (board[row, col] == 1)
            {
                PrintSolution(board);
            }

        }

        // Driver code  
        public static void Main()
        {
            Console.WriteLine("Please enter a valid chess coordinate where you want to place the queen");
            String userInput = Console.ReadLine();

            SolveNQ(userInput);

        }
    }
}
